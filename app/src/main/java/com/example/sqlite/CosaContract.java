package com.example.sqlite;

import android.provider.BaseColumns;

public class CosaContract {
    private CosaContract() {}

    public static class CosaEntry implements BaseColumns {
        public static final String TABLE_NAME = "cosas";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "nombre";
    }
}
