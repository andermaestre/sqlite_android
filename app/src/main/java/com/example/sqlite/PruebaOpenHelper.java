package com.example.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class PruebaOpenHelper extends SQLiteOpenHelper {
    Context ccc;
    public PruebaOpenHelper(@Nullable Context context) {

        super(context, "prueba.db", null, 1);
        this.ccc = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CosaContract.CosaEntry.TABLE_NAME + " (" +
                CosaContract.CosaEntry._ID + " INTEGER PRIMARY KEY," +
                CosaContract.CosaEntry.COLUMN_NAME + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CosaContract.CosaEntry.TABLE_NAME);
        onCreate(db);
    }
    public void meterCosa(int id, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id",id);
        cv.put("nombre",name);
        db.insert("cosas",null,cv);
        Toast.makeText(this.ccc,"Bien hecho",Toast.LENGTH_LONG).show();
    }
}
